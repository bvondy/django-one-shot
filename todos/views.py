from django.shortcuts import render
from django.views.generic import ListView
from todos.models import ToDoList

# Create your views here.


class ToDoListView(ListView):
    model = ToDoList
    template_name = "todos/list.html"
