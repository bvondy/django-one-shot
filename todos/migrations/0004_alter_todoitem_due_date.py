# Generated by Django 4.1.1 on 2022-09-07 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_alter_todoitem_due_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateField(null=True),
        ),
    ]
