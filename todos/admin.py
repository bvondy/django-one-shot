from django.contrib import admin
from todos.models import ToDoList, ToDoItem

# Register your models here.


class ToDoListAdmin(admin.ModelAdmin):
    pass


class ToDoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(ToDoList, ToDoListAdmin)
admin.site.register(ToDoItem, ToDoItemAdmin)
